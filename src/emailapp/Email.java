package emailapp;

import java.util.Scanner;

public class Email {
    // don't want people to access these, so use incapsulation (make private)
    private String firstName;
    private String lastName;
    private String password;
    private String department;
    private String email;
    private int mailboxCapacity = 500;
    private String alternateEmail;
    private int defaultPasswordLength = 10;
    private String companySuffix = "abcdcompany.com";

    //Constructor to receive the first name and last name
    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

        //Call a method asking for the department - return department
        this.department = setDepartment();
        
        //Call a method that returns a random password
        this.password = randomPassword(defaultPasswordLength);
        

        //Combine elements to generate email
        this.email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department + companySuffix;
        System.out.println("Your email is: " + email);
        System.out.println("Your password is: " + this.password);
    }

    //Ask for the department
    private String setDepartment() {
        System.out.print("New employee: " + firstName + " " + lastName + ".\nDepartment Codes\n1 for Sales\n2 for Development\n3 for Accounting\n0 for none\nEnter department code: ");
        Scanner input = new Scanner(System.in);
        int departmentChoice = input.nextInt();

        if (departmentChoice == 1) { return "sales."; } 
        else if (departmentChoice == 2) { return "dev."; } 
        else if (departmentChoice == 3) { return "acct."; }
        else { return ""; }

    }

    //Generate a random password
    private String randomPassword(int length) {
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@$#%";
        char[] password = new char[length];
        for (int i = 0; i < length; i++) {
            int random = (int) (Math.random() * passwordSet.length());
            /* 
            if there is 30 characters in passwordSet, then we want to generate a random number
            Math.random() works for values between 0 and 1, so we multiply that by the length
            of passwordSet to get a random number between 0 and 30
            */
            password[i] = passwordSet.charAt(random);
        }
        return new String(password); //Generate a String to return from the array
    }


    //Set the mailbox capacity -- making it public to access and change
    public void setMailboxCapacity(int capacity) {
        this.mailboxCapacity = capacity;        
    }

    //Set the alternate email
    public void setAltEmail(String altEmail) {
        this.alternateEmail = altEmail;
    }

    //Change the password
    public void changePassword(String password) {
        this.password = password;
    }

    // some getters
    public int getMailboxCapacity() { return mailboxCapacity; }

    public String getAlternateEmail() { return alternateEmail; }

    public String getPassword() { return password; }

    public String showInfo() {
        return "Display Name: " + firstName + " " + lastName +
            "\nCompany Email: " + email +
            "\nMailbox capacity: " + mailboxCapacity + "mb";
    }
}